package com.facebook.hackathon.localnews;

/**
 * Created by Diego on 08/11/17.
 */

public class News {

    private String titulo;
    private String alerta;
    private String categoria;
    private String descricao;
    private Local local;

    public News(String titulo, String alerta, String categoria, String descricao, Local local) {
        this.titulo = titulo;
        this.alerta = alerta;
        this.categoria = categoria;
        this.descricao = descricao;
        this.local = local;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAlerta() {
        return alerta;
    }

    public void setAlerta(String alerta) {
        this.alerta = alerta;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Local getLocal() {
        return local;
    }

    public void setLocal(Local local) {
        this.local = local;
    }
}


package com.facebook.hackathon.localnews;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Diego on 08/11/17.
 */
public class FeedAdapter extends BaseAdapter {

    private Activity activity;
    private List<News> feed = new ArrayList<News>();
    private TextView titulo;
    private TextView desc;

    public FeedAdapter(List<News> feed, Activity act) {
        this.feed = feed;
        this.activity =  act;
    }

//    @Override
//    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//
//        return ;
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return super.getItemId(position);
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//        return super.getItemViewType(position);
//    }
//
//    @Override
//    public void getVi(RecyclerView holder, int position) {
//
//        titulo.findViewById(R.id.titulo);
//        desc.findViewById(R.id.descricao);
//
//        feed.get(position);
//
//        titulo.setText(feed.get(position).getTitulo());
//        desc.setText(feed.get(position).getDescricao());
//
//    }

    @Override
    public int getCount() {
        return feed.size();
    }

    @Override
    public Object getItem(int i) {
        return this;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        activity.getLayoutInflater();

        titulo.findViewById(R.id.titulo);
        desc.findViewById(R.id.descricao);
        feed.get(position);
        titulo.setText(feed.get(position).getTitulo());
        desc.setText(feed.get(position).getDescricao());
        return null;
    }
}

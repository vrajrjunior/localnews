package com.facebook.hackathon.localnews;

/**
 * Created by Diego on 08/11/17.
 */

public class Local {

    private String localizacao;
    private String endereco;
    private String tipo;

    public Local(String localizacao, String endereco, String tipo) {
        this.localizacao = localizacao;
        this.endereco = endereco;
        this.tipo = tipo;
    }

    public String getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
